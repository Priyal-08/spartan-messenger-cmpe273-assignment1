from __future__ import print_function
from os import system
import grpc
import threading
import time
import signal
import sys
import spartan_messenger_pb2 as proto
import spartan_messenger_pb2_grpc as stub
import yaml
import base64
from Crypto.Cipher import AES
from Crypto import Random

BLOCKSIZE = 16
pad = lambda s: s + (BLOCKSIZE - len(s) % BLOCKSIZE) * chr(BLOCKSIZE - len(s) % BLOCKSIZE)
unpad = lambda s: s[:-ord(s[len(s) - 1:])]

class SpartanClient:
    def __init__(self):
        try:
            with open("config/client_config.yaml", "r") as configFile:
                config = yaml.load(configFile)
            port = config['port'] if config['port'] else 3000
            configFile.close()
            channel = grpc.insecure_channel('localhost:{}'.format(port))
            self.stub = stub.SpartanMessengerStub(channel)
            self.signal = signal.signal(signal.SIGINT, self.signalHandler)
            self.userId = sys.argv[1]
            self.MsgRate = 0
            self.user_keys = {}
            self.groupId = None
            with open("config/{}.yaml".format(self.userId), "r") as configFile:
                keyConfig = yaml.load(configFile)
            configFile.close()
            for userId in keyConfig["users"]:
                self.user_keys[userId] = keyConfig["users"][userId]['key']
            self.login()
        except FileNotFoundError:
            print("[Spartan] Sorry you can not login right now. Please check with administrator.")
            exit(0)
        except grpc._channel._Rendezvous:
            print("[Spartan] Spartan server is down. Please try after sometime.")
            exit(0)
        except KeyboardInterrupt:
            exit(0)

    def encrypt(self, message, key):
        message = pad(message)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(key, AES.MODE_CBC, iv )
        return base64.b64encode(iv + cipher.encrypt(message))


    def decrypt(self, message, key):
        message = base64.b64decode(message)
        iv = message[:16]
        cipher = AES.new(key, AES.MODE_CBC, iv )
        return unpad(cipher.decrypt(message[16:])).decode('utf-8')

    def login(self):
        response = self.stub.Login(proto.LoginUserRequest(userId=self.userId))
        if response.success:
            system('clear')
            print(response.message)
            self.showUsers()
        else:
            print(response.message)

    def showUsers(self):
        userList = self.stub.ShowUsers(proto.ShowUsersRequest(userId=self.userId))
        if len(userList.users)<1 and len(userList.groups)<1:
            print("[Spartan] Sorry, no other users have registered right now")
        else:
            if len(userList.users):
                users = ''
                for user in userList.users:
                    users += user.userId + ','
                users = users[:-1]
                print('[Spartan] Users: [{}]'.format(users))
            if len(userList.groups):
                groups = ''
                for group in userList.groups:
                    groups += group.groupId + ','
                groups = groups[:-1]
                print('[Spartan] Groups: [{}]'.format(groups))
            self.requestConnection()

    def requestConnection(self):
        chatUserId = input("[Spartan] Enter the userId/groupId that you would like to connect to: ")
        response = self.stub.ConnectUsers(proto.ConnectionRequest(senderId=self.userId, receiverId=chatUserId))
        if response.success:
            print("[Spartan] You are connected to {}.".format(chatUserId))
            self.inputMessage(chatUserId)
        else:
            if response.errorId == 1:
                print(response.error)
                self.showUsers()
            else:
                print(response.error)
                choice = input("[Spartan] Do you want to continue with sending offline message to this user? (y/n):")
                if choice in ('y', 'Y'):
                    self.inputMessage(chatUserId)
                else:
                    system('clear')
                    self.showUsers()

    def inputMessage(self, sendToUserId):
        self.getPastMsgs()
        threading.Thread(target=self.listenForMessages, daemon=True).start()
        while True:
            message = input("[{}] ".format(self.userId))
            if message is not '':
                self.sendMessage(sendToUserId, message)

    def sendMessage(self, sendToUserId, message):
        message = self.encrypt(message, self.user_keys[sendToUserId])
        m = proto.MessageRequest(senderId=self.userId, receiverId=sendToUserId, message=message)
        response = self.stub.SendMessage(m)
        if not response.success:
            print(response.error)

    def getPastMsgs(self):
        MessageList = self.stub.GetAllPastMessages(proto.User(userId = self.userId))
        if len(MessageList.messages):
            print("[Spartan] :: Chat history ::")
            for message in MessageList.messages:
                if message.receiverId in self.user_keys.keys():
                    key = self.user_keys[message.receiverId]
                elif message.senderId in self.user_keys.keys():
                    key = self.user_keys[message.senderId]
                print("[{}] {}".format(message.senderId, self.decrypt(message.message, key)))
        else:
            print("[Spartan] :: No Chat history ::")

    def listenForMessages(self):
        try:
            for message in self.stub.ChatStream(proto.User(userId = self.userId)):
                if message.senderId != self.userId:
                    if message.receiverId in self.user_keys.keys():
                        key = self.user_keys[message.receiverId]
                    elif message.senderId in self.user_keys.keys():
                        key = self.user_keys[message.senderId]
                    print("[{}] {}".format(message.senderId, self.decrypt(message.message, key)))
        except grpc._channel._Rendezvous:
            exit(0)
        except KeyboardInterrupt:
            exit(0)

    def signalHandler(self, sig, frame):
        try:
            response = self.stub.Logout(proto.LogoutUserRequest(userId=self.userId))
            if response.success:
                print("[Spartan] Successfully logged-out")
            exit(0)
        except grpc._channel._Rendezvous:
            exit(0)
        except KeyboardInterrupt:
            exit(0)


if __name__ == '__main__':
    c = SpartanClient()
