from concurrent import futures
from enum import Enum
import time
import yaml
import grpc
import signal
import spartan_messenger_pb2 as proto
import spartan_messenger_pb2_grpc as stub
from cache_list import cache_list

_ONE_DAY_IN_SECONDS = 60 * 60 * 24
rate_dict = {}
global rate_limit
global port

class Status(Enum):
    ACTIVE = 1
    INACTIVE = 2
    BUSY = 3

def RateLimiter(func):
    def helper(self, request, connId, rateLimitExceeded):
        global rate_limit
        id = request.senderId
        if id in rate_dict.keys():
            if rate_dict[id]["rate"] < rate_limit:
                rate_dict[id]["rate"] += 1
            else:
                elapsedTime = time.time() - rate_dict[id]["lastCalled"]
                if elapsedTime < 30:
                    rateLimitExceeded = True
                else:
                    rate_dict[id]["rate"] = 1
                    rate_dict[id]["lastCalled"] = time.time()
        else:
            rate_dict[id] = {"lastCalled": time.time(), "rate": 1}
        return func(self, request, connId, rateLimitExceeded)
    return helper

class SpartanMessenger(stub.SpartanMessengerServicer):
    def __init__(self):
        with open("config/config.yaml", "r") as configFile:
            config = yaml.load(configFile)
        configFile.close()
        self.user_dict = {}
        self.chat_dict = {}
        self.group_dict = {}
        self.user_connection_reference = {}
        self.connectionId = 0
        for userId in config["users"]:
            self.user_dict[userId] = {"status": Status.INACTIVE, "connectedTo": '', "connectionId": 0, "lastMsgIdx": 0, "lastMsgId": 0, "group": None}
        for groupId in config["groups"]:
            self.group_dict[groupId] = []
            for user in config["groups"][groupId]:
                self.user_dict[user]["group"] = groupId
                self.group_dict[groupId].append(user)
        self.lru_cache_size = config['max_num_messages_per_user'] if config['max_num_messages_per_user'] else 5

    def Login(self, request, context):
        if request.userId not in self.user_dict:
            return proto.LoginResponse(success=False, message="[Spartan] Login error: User Id does not exist.")
        elif self.user_dict[request.userId]["status"] == Status.ACTIVE:
            return proto.LoginResponse(success=False, message="[Spartan] Login error: Already logged-in")
        else:
            self.user_dict[request.userId]["status"] = Status.ACTIVE
            message = "[Spartan] Connected to Spartan Server at port {}.".format(port)
            return proto.LoginResponse(success=True, message=message)

    def Logout(self, request, context):
        self.user_dict[request.userId]["status"] = Status.INACTIVE
        self.user_dict[request.userId]["connectedTo"] = ''
        self.user_dict[request.userId]["connectionId"] = 0
        self.user_dict[request.userId]["lastMsgIdx"] = 0
        self.user_dict[request.userId]["lastMsgId"] = 0
        return proto.GeneralResponse(success=True, error="")

    def ShowUsers(self, request, context):
        users = []
        for userId in self.user_dict.keys():
            if userId != request.userId:
                users.append(proto.User(userId=userId))
        groups = []
        for groupId in self.group_dict.keys():
            if request.userId in self.group_dict[groupId]:
                groups.append(proto.Group(groupId=groupId))
        return proto.UserList(users=users,groups=groups)

    def ConnectUsers(self, request, context):
        if request.receiverId in self.group_dict.keys():
            if (request.senderId, request.receiverId) not in self.user_connection_reference.keys():
                self.connectionId += 1
                for user in self.group_dict[request.receiverId]:
                    self.user_connection_reference[(user, request.receiverId)] = self.connectionId
                    self.user_connection_reference[(request.receiverId, user)] = self.connectionId
                self.chat_dict[self.connectionId] = cache_list(self.lru_cache_size)
            self.user_dict[request.senderId]["status"] = Status.BUSY
            self.user_dict[request.senderId]["connectedTo"] = request.receiverId
            self.user_dict[request.senderId]["connectionId"] = self.user_connection_reference[(request.receiverId, request.senderId)]
            return proto.GeneralResponse(success=True, error="")
        elif request.receiverId not in self.user_dict.keys():
            return proto.GeneralResponse(success=False, error="[Spartan] User-ID {} does not exist".format(request.receiverId), errorId=1)
        elif(request.senderId, request.receiverId) not in self.user_connection_reference.keys():
            self.connectionId += 1
            self.user_connection_reference[(request.senderId, request.receiverId)] = self.connectionId
            self.user_connection_reference[(request.receiverId, request.senderId)] = self.connectionId
            self.chat_dict[self.connectionId] = cache_list(self.lru_cache_size)
        self.user_dict[request.senderId]["status"] = Status.BUSY
        self.user_dict[request.senderId]["connectedTo"] = request.receiverId
        self.user_dict[request.senderId]["connectionId"] = self.user_connection_reference[(request.receiverId, request.senderId)]
        if self.user_dict[request.receiverId]["connectedTo"] == request.senderId:
            return proto.GeneralResponse(success=True)
        else:
            return proto.GeneralResponse(success=False, error="[Spartan] {} is not available right now.".format(request.receiverId),errorId=2)

    def SendMessage(self, request, context):
        connId = self.user_connection_reference[(request.senderId, request.receiverId)]
        response = self.AddMessageInChannel(request, connId, False)
        if response:
            return proto.GeneralResponse(success=True, error="")
        else:
            return proto.GeneralResponse(success=False, error="[Spartan] Last message could not be sent. Please resend after sometime")

    @RateLimiter
    def AddMessageInChannel(self, request, connId, rateLimitExceeded):
        response = False
        if not rateLimitExceeded:
            response = True
            self.chat_dict[connId].append(request)
        return response

    def GetAllPastMessages(self, request, context):
        idx = 0
        lastMsgId = 0
        lastConnectionId = 0
        messages = []
        while True:
            # Check if client is connected to any user
            connId = self.user_dict[request.userId]["connectionId"]
            if connId != 0:
                if connId != lastConnectionId:
                    lastConnectionId = connId
                    idx = self.chat_dict[connId].getOldestMsgIdx()
                response = self.chat_dict[connId].get(idx, lastMsgId)
                if response is not None:
                    message = response["message"]
                    lastMsgId = response["msgId"]
                    if idx == self.lru_cache_size - 1:
                        idx = 0
                    else:
                        idx += 1
                    messages.append(message)
                else:
                    break
        self.user_dict[request.userId]["lastMsgIdx"] = idx
        self.user_dict[request.userId]["lastMsgId"] = lastMsgId
        return proto.MessageList(messages=messages)

    def ChatStream(self, request, context):
        idx = self.user_dict[request.userId]["lastMsgIdx"]
        lastMsgId = self.user_dict[request.userId]["lastMsgId"]
        while True:
            try:
                # Check if client is connected to any user
                connId = self.user_dict[request.userId]["connectionId"]
                if connId != 0:
                    response = self.chat_dict[connId].get(idx, lastMsgId)
                    if response is not None:
                        message = response["message"]
                        lastMsgId = response["msgId"]
                        if idx == self.lru_cache_size - 1:
                            idx = 0
                        else:
                            idx += 1
                        yield message
            except Exception:
                break

def signalHandler(sig, frame):
    exit(0)

def serve():
    global rate_limit
    global port
    with open("config/config.yaml", "r") as configFile:
            config = yaml.load(configFile)
    port = config['port'] if config['port'] else 3000
    max_workers = config['max_workers'] if config['max_workers'] else 10
    rate_limit = config['max_call_per_30_seconds_per_user'] if config['max_call_per_30_seconds_per_user'] else 3
    print(rate_limit)
    configFile.close()
    signal.signal(signal.SIGINT, signalHandler)
    server = grpc.server(futures.ThreadPoolExecutor(max_workers))
    stub.add_SpartanMessengerServicer_to_server(SpartanMessenger(), server)
    server.add_insecure_port('[::]:{}'.format(port))
    server.start()
    print("Spartan server started on port {}.".format(port))
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)

if __name__ == '__main__':
    serve()
