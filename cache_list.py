class cache_list:
	def __init__(self, maxSize):
		self.maxSize = int(maxSize)
		self.lru_list = [{"msgId":0,"message":None}] * self.maxSize
		self.lastIdx = -1
		self.nextMsgId = 0

	#decorator
	def lru_cache(func):
		def helper(self, message):
			if self.lastIdx == self.maxSize - 1:
				self.lastIdx = 0
			else:
				self.lastIdx = self.lastIdx+1
			return func(self,message)
		return helper

	@lru_cache
	def append(self, message):
		self.nextMsgId += 1
		self.lru_list[self.lastIdx] = {"msgId":self.nextMsgId,"message":message}

	def get(self,idx,lastMsgId):
		if self.lru_list[idx]["msgId"] > lastMsgId:
			return self.lru_list[idx]
		else:
			return None

	def getOldestMsgIdx(self):
		i = 0
		minMsgId = self.lru_list[0]["msgId"]
		minIdx = 0
		while i < self.maxSize -1:
			if minMsgId > self.lru_list[i]["msgId"] != 0:
				minMsgId = self.lru_list[i]["msgId"]
				minIdx = i
			i += 1
		return minIdx
