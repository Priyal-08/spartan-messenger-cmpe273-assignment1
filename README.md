**Spartan Messenger**

A response-streaming RPC where the client sends a request to the server and gets a stream to read a sequence of messages back. The client reads from the returned stream until there are no more messages.

---

## Prerequisite

Python3

---

## Installations

1. pip3 install grpcio
2. pip3 install grpcio-tools googleapis-common-protos
3. pip3 install pycrypto
4. pip3 install PyYAML

---

## How to run the server

python3 server.py 

Sample Output: 
$ python3 server.py
Spartan server started on port 3000.

---

## How to run the client

python3 client.py <user-id> 

Sample Output: 
$ python3 client.py emma

[Spartan] Connected to Spartan Server.

[Spartan] Users: [alice,bob,charlie,eve,foo,bar,baz,qux,jorge,]

[Spartan] Enter the userId of user that you would like to connect to: jorge

